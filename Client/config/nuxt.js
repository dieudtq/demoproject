const pkg = require('../package')
const resolve = require('path').resolve
const Env = use('Env')

module.exports = {
  mode: 'spa',

  srcDir: resolve(__dirname, '..', 'resources'),

  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: '%s | Nail eCommerce',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css?family=Poppins:300,700' },
      { rel: 'stylesheet', type: 'text/css', href: '/fonts/font-awesome/css/font-awesome.min.css' },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: '~/components/Loading',

  /*
  ** Global CSS
  */
  css: [
    '~/scss/style.scss'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    {
      src: '~/plugins/axios',
      ssr: false
    },
    {
      src: '~/plugins/element-ui',
      ssr: false
    },
    {
      src: '~/plugins/nuxt-client-init',
      ssr: false
    },
    {
      src: '~/plugins/vue-lazy-load',
      ssr: false
    },
    {
      src: '~/plugins/vue-notification',
      ssr: false
    },
    {
      src: '~/plugins/vee-validate',
      ssr: false
    },
    {
      src: '~/plugins/v-mask',
      ssr: false
    },
    {
      src: '~/plugins/lodash',
      ssr: false
    },
    {
      src: '~/plugins/vue-line-clamp',
      ssr: false
    }
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    // Doc: https://bootstrap-vue.js.org/docs/
    'bootstrap-vue/nuxt'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: Env.get('SERVER_URL'),
    host: Env.get('SERVER_HOST'),
    post: Env.get('SERVER_PORT'),
    prefix: Env.get('PREFIX')
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      
    }
  },
  env: {
    fbAppId: Env.get('FB_APP_ID'),
    fbPageId: Env.get('FB_PAGE_ID'),
    captchaSitekey: Env.get('CAPTCHA_SITEKEY'),
    paypalEnv: Env.get('PAYPAL_ENV')
  },
  router: {
    extendRoutes (routes, resolve) {
      routes.push({
        name: 'custom',
        path: '*',
        component: resolve(__dirname, '@/pages/404.vue')
      })
    }
  }
}
