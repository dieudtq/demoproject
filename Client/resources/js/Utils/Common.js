export const showPage = (totalPage, pagination, i) => {
  if(pagination.currentPage === i + 1) {
    return true
  }

  if(pagination.currentPage === 1) {
    if (i + 1 <= pagination.display) {
      return true
    } else {
      return false
    }
  } else if(pagination.currentPage === totalPage) {
    if (i + 1 >= totalPage - pagination.display + 1) {
      return true
    } else {
      return false
    }
  } else {
    const range = Math.floor(pagination.display / 2)
    let min = 0
    let max = 0

    if(pagination.currentPage - range < 1) {
      min = 1
      max = pagination.display
    } else if(pagination.currentPage + range > totalPage) {
      min = totalPage - pagination.display + 1
      max = totalPage
    } else {
      min = pagination.currentPage - range
      max = pagination.currentPage + range
    }

    if (i + 1 >= min && i + 1 <= max) {
      return true
    } else {
      return false
    }
  }
}