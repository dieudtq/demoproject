export default class Product {
  constructor(ctx) {
    this.ctx = ctx
  }

  addFavorite(id) {
    return new Promise((resolve, reject) => {
      this.ctx.$axios.post(`wishlist/${id}`).then(res => {
        resolve(res.data)
      }).catch(err => {
        reject()
      })
    })
  }

  removeFavorite(id) {
    return new Promise((resolve, reject) => {
      this.ctx.$axios.delete(`wishlist/${id}`).then(res => {
        resolve(res.data)
      }).catch(err => {
        reject()
      })
    })
  }

  removeViewHistory(id) {
    return new Promise((resolve, reject) => {
      this.ctx.$axios.delete(`myviewhistory/${id}`).then(res => {
        resolve(res.data)
      }).catch(err => {
        reject()
      })
    })
  }

  removeAllViewHistory() {
    return new Promise((resolve, reject) => {
      this.ctx.$axios.delete(`myviewhistory`).then(res => {
        resolve(res.data)
      }).catch(err => {
        reject()
      })
    })
  }
}