import Vue from 'vue'
import VueLazyload from 'vue-lazyload'

Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: '/images/icon/error.jpg',
  loading: '/images/icon/loading.svg',
  attempt: 1,
  observer: true,
  lazyComponent: true
})