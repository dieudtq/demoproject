import Vue from 'vue'
import VeeValidate from 'vee-validate'

const config = {
  classNames: {
    invalid: 'is-invalid'
  },
  events: 'input|blur'
}

Vue.use(VeeValidate, config)