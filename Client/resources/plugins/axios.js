export default function ({ $axios, redirect, store, app }) {
  $axios.onRequest(config => {
    if (store.state.auth) {
      config.headers.common['Authorization'] = `Bearer ${store.state.auth}`
    }

    store.commit('loading/setLoading', true)
  })

  $axios.onResponse(response => {
    store.commit('loading/setLoading', false)
  })

  $axios.onError(error => {
    try {
      store.commit('loading/setLoading', false)
      const { status, data } = error.response

      if(status === 500) {
        if(data.path) {
          redirect(data.path)
        } else {
          redirect('/500')
        }
      }

      if(status === 404) {
        redirect('/404')
      }

      if(status === 401) {
        redirect('/logout')
      }
    } catch (e) {
      redirect('/maintenance')
    }
  })
}