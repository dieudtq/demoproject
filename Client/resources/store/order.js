export const state = () => ({
  order: []
})

export const mutations = {
  addOrder(state, data) {
    state.order = data
  },
  addProducts(state, data) {
    state.order.list_product.products = [...state.order.list_product.products, ...data.products]
    state.order.list_product.next = data.next
    ++state.order.list_product.page
  }
}