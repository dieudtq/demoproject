export const state = () => ({
  user: {}
})

export const mutations = {
  addUser(state, user) {
    state.user = user
  }
}

export const actions = {
  addUser({commit}, user) {
    commit('addUser', user)
  }
}

export const getters = {
  getUser: state => state.user
}
