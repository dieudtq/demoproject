export const state = () => ({
  status: false
})

export const mutations = {
  SET_STATUS(state, status) {
    state.status = status
  }
}

export const actions = {
  setStatus({commit}, data) {
    commit('SET_STATUS', data)
  }
}
