export const state = () => ({
  cart: [],
  count: 0,
  step: 1
})

export const mutations = {
  setCart(state, data) {
    let customProps = null
    let products = null
    if(data) {
      products = data.products.map(item => {
        let stateCustom = 0
        if(item.max === 0) {
          stateCustom = 2
        } else {
          stateCustom = 0
        }
        let quantityCustom = item.quantity > item.max ? item.max : item.quantity
  
        customProps = {
          state: stateCustom,
          quantity: quantityCustom
        }
        return Object.assign({}, item, customProps)
      })
    }

    state.cart = Object.assign({}, data, {
      products
    })
  },
  setShipping(state, data) {
    state.cart = Object.assign({}, state.cart, data)
  },
  setPayment(state, data) {
    state.cart = Object.assign({}, state.cart, {
      payment: data
    })
  },
  setFinish(state, data) {
    state.cart = Object.assign({}, state.cart, {
      finish: data
    })
  },
  addCart(state, obj) {
    state.cart.products.push(obj)
  },
  removeCart(state, index) {
    state.cart.products.splice(index, 1)
  },
  changeQuantity(state, { index, quantity, stateCustom, max }) {
    state.cart.products[index].state = stateCustom

    switch(stateCustom) {
      case 1:
        state.cart.products[index].max = max
        break
      case 2:
        state.cart.products[index].max = 0
        state.cart.products[index].quantity = 0
        break
      default:
        state.cart.products[index].quantity = quantity
        state.cart.products[index].max = max
    }
  },
  setCount(state, val) {
    state.count = val
  },
  setStep(state, val) {
    state.step = val
  }
}

export const getters = {
  getCount: state => state.count
}
