export const state = () => ({
  products: [],
  name: '',
  paging: {},
  rangePrice: [0, 1000],
  priceMax: 1000,
  category: [],
  categories: [],
  ads: {}
})

export const mutations = {
  addData(state, data) {
    state.products = data.products
    state.name = data.name
    state.paging = data.paging
    state.priceMax = data.priceMax
    state.rangePrice = data.rangePrice
    state.ads = data.ads
  },
  addSubCategory(state, data) {
    state.category = data
  },
  addCategories(state, data) {
    state.categories = data
  }
}
