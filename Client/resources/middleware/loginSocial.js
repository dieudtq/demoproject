export default function ({ store, redirect }) {
  if(!store.state.login.type) {
    return redirect('/login')
  }
}