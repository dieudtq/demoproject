'use strict'
const Env = use('Env')
const paypal = require('paypal-rest-sdk')
paypal.configure({
  mode: Env.get('PAYPAL_ENV'), // Sandbox or live
  client_id: Env.get('PAYPAL_CLIENT'),
  client_secret: Env.get('PAYPAL_SECRET')
})

class PayPalController {
  async createPayment({ request, response}) {
    const payment = await this.create()
    response.send(payment)
  }

  async executePayment({ request, response}) {
    const body = request.post()
    const payerId = {
      payer_id: body.payerID
    }
    console.log(body)
    console.log(body.paymentID)
    console.log(payerId)
    const res = await this.execute(body.paymentID, payerId)
    if(res.status) {
      response.send({
        status: res.status,
        message: 'Payment successfully'
      })
    } else {
      response.send({
        status: res.status,
        message: 'Payment failed'
      })
    }
  }

  create() {
    return new Promise((resolve, reject) => {
      const payReq = JSON.stringify({
        intent: 'sale',
        payer:{
          payment_method:'paypal'
        },
        redirect_urls:{
          return_url:'http://tamnt.tamnt.work:3000/cart',
          cancel_url:'http://tamnt.tamnt.work:3000/'
        },
        transactions:[{
          amount:{
            total:'10',
            currency:'USD'
          },
          description:'This is the payment transaction description.'
        }]
      })

      paypal.payment.create(payReq, (error, payment) => {
        if(error) {
          reject(error)
        }

        resolve(payment)
      }) 
    })
  }

  execute(paymentId, payerId) {
    return new Promise((resolve, reject) => {
      paypal.payment.execute(paymentId, payerId, (error, payment) => {
        if(error){
          reject(error)
        } else {
          if (payment.state == 'approved'){
            resolve({
              status: true
            })
          } else {
            resolve({
              status: false
            })
          }
        }
      })
    })
  }
}

module.exports = PayPalController
